$(document).ready(function () {
    $('.dropdown-btn').on('click', function (event) {
        event.stopPropagation();
        let $container = $(this).closest('.dropdown-item');
        $container.toggleClass('open');
    });

    $('body').on('click', function () {
        if (window.innerWidth > 1260) {
            $('.dropdown-item').each(function () {
                let element = $(this);
                if (element.hasClass('open')) {
                    element.removeClass('open');
                }
            });
        }
    });

    $('.header__nav-mobile-btn').on('click', function () {
        $(this).toggleClass('active');
        $(this).parent().find('.header__nav').slideToggle('slow');
    });
});
